package src.services;

import java.util.Arrays;

public class Array3Fix34 {

    private int[] fix34(int[] nums) {
        int fourPos = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 3) {
                int temp = nums[i + 1];
                for (int j = fourPos; j < nums.length; j++) {
                    if (nums[j] == 4) {
                        nums[j] = temp;
                        fourPos = j + 1;
                        j = nums.length;
                    }
                }
                nums[i + 1] = 4;
            }
        }
        return nums;
    }

    public void execute() {
        System.out.println(Arrays.toString(fix34(new int[]{1, 3, 1, 4})));
        System.out.println(Arrays.toString(fix34(new int[]{1, 3, 1, 4, 4, 3, 1})));
        System.out.println(Arrays.toString(fix34(new int[]{3, 2, 2, 4})));
    }
}
