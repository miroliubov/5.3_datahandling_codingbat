package src.services;

public class String3GHappy {

    private boolean gHappy(String str) {
        boolean m = true;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'g') {
                if (i > 0 && str.charAt(i-1) == 'g') {
                    m = true;
                }
                else m = i < str.length() - 1 && str.charAt(i + 1) == 'g';
            }
        }
        return m;
    }

    public void execute() {
        System.out.println(gHappy("xxggxx"));
        System.out.println(gHappy("xxgxx"));
        System.out.println(gHappy("xxggyygxx"));
    }

}
