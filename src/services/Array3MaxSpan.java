package src.services;

public class Array3MaxSpan {

    private int maxSpan(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int maxspan = 1;
        for (int i = 0; i < nums.length; i++) {

            for (int j = 1; j < nums.length; j++) {
                int span = 1;
                if (nums[i] == nums[j]) {
                    span = j - i + 1;
                }
                if (span > maxspan) {
                    maxspan = span;
                }
            }
        }
        return maxspan;
    }

    public void execute() {
        System.out.println(maxSpan(new int[]{1, 2, 1, 1, 3}));
        System.out.println(maxSpan(new int[]{1, 4, 2, 1, 4, 1, 4}));
        System.out.println(maxSpan(new int[]{1, 4, 2, 1, 4, 4, 4}));
    }
}
