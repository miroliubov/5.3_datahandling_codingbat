package src.services;

public class String3SumDigits {

    private int sumDigits(String str) {
        int sum = 0;
        str = str.replaceAll("[^0-9]", "");
        for (int i = 0; i < str.length() ; i++) {
            sum = sum + Character.getNumericValue(str.charAt(i));
        }
        return sum;
    }

    public void execute() {
        System.out.println(sumDigits("aa1bc2d3"));
        System.out.println(sumDigits("aa11b33"));
        System.out.println(sumDigits("Chocolate"));
    }
}
