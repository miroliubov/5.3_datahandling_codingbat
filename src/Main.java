package src;

import src.services.Array3Fix34;
import src.services.Array3MaxSpan;
import src.services.String3GHappy;
import src.services.String3SumDigits;

public class Main {

    public static void main(String[] args) {
        System.out.println("String 3, gHappy");
        String3GHappy gHappy = new String3GHappy();
        gHappy.execute();

        System.out.println("String 3, sumDigits");
        String3SumDigits sumDigits = new String3SumDigits();
        sumDigits.execute();

        System.out.println("Array 3, maxSpan");
        Array3MaxSpan maxSpan = new Array3MaxSpan();
        maxSpan.execute();

        System.out.println("Array 3 fix34");
        Array3Fix34 fix34 = new Array3Fix34();
        fix34.execute();
    }
}
